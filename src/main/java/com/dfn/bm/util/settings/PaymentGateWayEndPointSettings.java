package com.dfn.bm.util.settings;

public class PaymentGateWayEndPointSettings {

    private String host;
    private int port;
    private boolean sameHost;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public boolean isSameHost() {
        return sameHost;
    }

    public void setSameHost(boolean sameHost) {
        this.sameHost = sameHost;
    }
}
