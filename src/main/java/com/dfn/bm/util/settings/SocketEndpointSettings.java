package com.dfn.bm.util.settings;

import com.dfn.bm.util.SecureConnectionSettings;

public class SocketEndpointSettings {

    private String server = "localhost";
    private int port = 8085;
    private boolean autoTimeout = true;
    private int timeout = 300;
    private boolean secureConnection =false;
    private SecureConnectionSettings secureConnectionSettings;

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public boolean isAutoTimeout() {
        return autoTimeout;
    }

    public void setAutoTimeout(boolean autoTimeout) {
        this.autoTimeout = autoTimeout;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public boolean isSecureConnection() {
        return secureConnection;
    }

    public void setSecureConnection(boolean secureConnection) {
        this.secureConnection = secureConnection;
    }

    public SecureConnectionSettings getSecureConnectionSettings() {
        return secureConnectionSettings;
    }

    public void setSecureConnectionSettings(SecureConnectionSettings secureConnectionSettings) {
        this.secureConnectionSettings = secureConnectionSettings;
    }
}
