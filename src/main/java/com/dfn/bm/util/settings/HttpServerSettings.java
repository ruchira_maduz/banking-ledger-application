package com.dfn.bm.util.settings;

/**
 * Settings class for Server HttpServer settings of Recruitement Manager.
 */

public class HttpServerSettings {

    /**
     * IP address of Server httpServerSettings.
     */
    private String ip;

    /**
     *  default route Ip address of Server httpServerSettings.
     */

    private String defaultIp;

    /**
     * Web Socket Port of Server httpServerSettings.
     */
    private String port;

    //Domain of the server which Rec Manager is running
    private String domainUrl;

    //concatenated Url of the server with Port included  which Rec Manager is running
    private String url;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getDefaultIp() {
        return defaultIp;
    }

    public void setDefaultIp(String defaultIp) {
        this.defaultIp = defaultIp;
    }


    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getDomainUrl() {
        return domainUrl;
    }

    public void setDomainUrl(String domainUrl) {
        this.domainUrl = domainUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
