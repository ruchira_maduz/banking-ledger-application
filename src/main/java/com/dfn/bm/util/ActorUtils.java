package com.dfn.bm.util;

import akka.actor.typed.ActorRef;
import com.dfn.bm.util.message.Command;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Utility class related to handling Actor System based utility functions.
 */
public final class ActorUtils {

    private static final Logger LOGGER = LogManager.getLogger(ActorUtils.class);
    private static akka.actor.typed.ActorSystem<Command> rootSupervisor;
    private static akka.actor.typed.ActorRef<Command> transactionEndPointSupervisor;
    private static akka.actor.typed.ActorRef<Command> loginLogoutEndPointSupervisor;
    private static akka.actor.typed.ActorRef<Command> utilityEndPointSupervisor;

    private static AtomicLong atomicLong = new AtomicLong();

    private ActorUtils() {

    }

    public static String generateUniqueId() {
        long timeID = System.currentTimeMillis();
        long uniqueID = atomicLong.incrementAndGet();
        String nodeID = "A";
        return "" + nodeID + "" + timeID + "" + uniqueID;
    }


    public static akka.actor.typed.ActorRef<Command> getTransactionEndPointSupervisor() {
        return transactionEndPointSupervisor;
    }

    public static void setTransactionEndPointSupervisor(akka.actor.typed.ActorRef<Command> transactionEndPointSupervisor) {
        ActorUtils.transactionEndPointSupervisor = transactionEndPointSupervisor;
    }

    public static akka.actor.typed.ActorSystem<Command> getRootSupervisor() {
        return rootSupervisor;
    }

    public static void setRootSupervisor(akka.actor.typed.ActorSystem<Command> rootSupervisor) {
        ActorUtils.rootSupervisor = rootSupervisor;
    }

    public static ActorRef<Command> getLoginLogoutEndPointSupervisor() {
        return loginLogoutEndPointSupervisor;
    }

    public static void setLoginLogoutEndPointSupervisor(ActorRef<Command> loginLogoutEndPointSupervisor) {
        ActorUtils.loginLogoutEndPointSupervisor = loginLogoutEndPointSupervisor;
    }

    public static ActorRef<Command> getUtilityEndPointSupervisor() {
        return utilityEndPointSupervisor;
    }

    public static void setUtilityEndPointSupervisor(ActorRef<Command> utilityEndPointSupervisor) {
        ActorUtils.utilityEndPointSupervisor = utilityEndPointSupervisor;
    }
}
