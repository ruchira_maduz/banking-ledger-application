package com.dfn.bm.util.security;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;

public class PasswordEncryptor {

    private static final Logger LOGGER = LogManager.getLogger(PasswordEncryptor.class);


    private PasswordEncryptor() {

    }

    public static String getSha256SecurePassword(String passwordToHash, byte[] salt) {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(salt);
            byte[] bytes = md.digest(passwordToHash.getBytes());
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error(e);
        }
        return generatedPassword;
    }

    public static String getSha256SecurePassword(String passwordToHash) {

        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] bytes = md.digest(passwordToHash.getBytes());
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error(e);
        }
        return generatedPassword;
    }

    public static boolean verifyPassword(String password, String hashedPassword) throws Exception {
        List<String> split = Arrays.asList(hashedPassword.split("\\$"));
        if (getSha256SecurePassword(password).equals(hashedPassword)) {
            return true;
        }
        return false;
    }
}
