package com.dfn.bm.util;

public class HTTPEndpointSettings {

    private String server = "localhost";
    private int port = 8080;
    private int requestWorkerPoolSize = 100;
    private int responseWorkerPoolSize = 100;
    private boolean secureConnection =false;
    private SecureConnectionSettings secureConnectionSettings;

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getRequestWorkerPoolSize() {
        return requestWorkerPoolSize;
    }

    public void setRequestWorkerPoolSize(int requestWorkerPoolSize) {
        this.requestWorkerPoolSize = requestWorkerPoolSize;
    }

    public int getResponseWorkerPoolSize() {
        return responseWorkerPoolSize;
    }

    public void setResponseWorkerPoolSize(int responseWorkerPoolSize) {
        this.responseWorkerPoolSize = responseWorkerPoolSize;
    }

    public boolean getSecureConnection() {
        return secureConnection;
    }

    public void setSecureConnection(boolean secureConnection) {
        this.secureConnection = secureConnection;
    }

    public SecureConnectionSettings getSecureConnectionSettings() {
        return secureConnectionSettings;
    }

    public void setSecureConnectionSettings(SecureConnectionSettings secureConnectionSettings) {
        this.secureConnectionSettings = secureConnectionSettings;
    }
}
