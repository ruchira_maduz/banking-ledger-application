package com.dfn.bm.util;

import com.dfn.bm.util.exception.LedgerManagerRuntimeException;
import io.jsonwebtoken.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Key;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

public class JWTPIUtils {

    private static final Logger LOGGER = LogManager.getLogger(JWTPIUtils.class);

    private static final String DEFAULT_JWT_FILE = "./config/jwt.key";


    public static String createJWT(String subject,
                                   String issuer,
                                   long ttlMillis) {

        Path path = Paths.get(DEFAULT_JWT_FILE);
        String jwtSecret = null;
        try {
            jwtSecret = Files.readAllLines(path).get(0);
        } catch (IOException e) {
            LOGGER.error(e);
        }

        //The JWT signature algorithm we will be using to sign the token
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        //We will sign our JWT with our ApiKey secret
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(jwtSecret);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        //Let's set the JWT Claims
        JwtBuilder builder = Jwts.builder().setId(UUID.randomUUID().toString())
                .setIssuedAt(now)
                .setSubject(subject)
                .setIssuer(issuer)
                .signWith(signatureAlgorithm, signingKey);

        //if it has been specified, let's add the expiration
        if (ttlMillis >= 0) {
            long expMillis = nowMillis + ttlMillis;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp);
        }


        //Builds the JWT and serializes it to a compact, URL-safe string
        return builder.compact();
    }

    public static Jws<Claims> decodeJWT(String jwt) {

        Path path = Paths.get(DEFAULT_JWT_FILE);
        String jwtSecret = null;
        try {
            jwtSecret = Files.readAllLines(path).get(0);
        } catch (IOException e) {
            LOGGER.error(e);
        }

        //This line will throw an exception if it is not a signed JWS (as expected)

        Jws<Claims> claims = null;
        try {
            claims = Jwts.parser()
                    .setSigningKey(DatatypeConverter.parseBase64Binary(jwtSecret))
                    .parseClaimsJws(jwt);
        } catch (Exception e) {
            throw new LedgerManagerRuntimeException(e);
        }

        return claims;
    }

}
