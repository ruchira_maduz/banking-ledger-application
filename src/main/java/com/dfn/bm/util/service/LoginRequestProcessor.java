package com.dfn.bm.util.service;

import com.dfn.bm.actors.messages.ServicesBaseResponse;
import com.dfn.bm.beans.User;
import com.dfn.bm.connector.database.DBHandler;
import com.dfn.bm.util.Constants;
import com.dfn.bm.util.JWTPIUtils;
import com.dfn.bm.util.ServicesUtills;
import com.dfn.bm.util.security.PasswordEncryptor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import java.util.UUID;

public class LoginRequestProcessor implements ServiceProcessor {

    private static final Logger LOGGER = LogManager.getLogger(LoginRequestProcessor.class);

    private static LoginRequestProcessor INSTANCE = new LoginRequestProcessor();

    private LoginRequestProcessor() {

    }

    public static LoginRequestProcessor getInstance() {
        return INSTANCE;
    }

    @Override
    public ServicesBaseResponse process(Object params, int functionId) {
        ServicesBaseResponse servicesBaseResponse = new ServicesBaseResponse();
        JSONObject request = (JSONObject) params;
        String userName = request.get("userName").toString();
        String password = request.get("password").toString();

        String sessionId = JWTPIUtils.createJWT(Constants.JWT_SUBJECT, Constants.JWT_ISSUER, Constants.JWT_EXPIRY_PERIOD);
        if (validateLogin(password, userName)) {
            JSONObject object = new JSONObject();
            object.put("token", sessionId);

            ServicesUtills.generateSuccussResponse(servicesBaseResponse, object);
        } else {
            ServicesUtills.generateErrorResponse(servicesBaseResponse, "Invalid UserName Password");
        }

        return servicesBaseResponse;

    }

    private String generateSessionID() {
        return (UUID.randomUUID()).toString();
    }

    private boolean validateLogin(String enteredPassword, String userName) {
        boolean status = false;
        try {
            User object = DBHandler.getInstance().getUserObjectByUserName(userName);
            status = PasswordEncryptor.verifyPassword(enteredPassword, object.getPassword());
        } catch (Exception e) {
            LOGGER.error("Password Verification Error", e);
        }
        return status;
    }

}
