package com.dfn.bm.util.service;

import com.dfn.bm.util.exception.LedgerManagerRuntimeException;

public class ServicesProcessorFactory {
    public static ServiceProcessor getProcessor(String processorType) {

        if (processorType == null) {
            throw new LedgerManagerRuntimeException("Null is not accepted as processor type");
        }

        if (ProcessorTypeConstants.ProcessorTypes.UTILITY.toString().equalsIgnoreCase(processorType)) {
            return UtilityProcessor.getInstance();
        } else if (ProcessorTypeConstants.ProcessorTypes.LOGIN.toString().equalsIgnoreCase(processorType)) {
            return LoginRequestProcessor.getInstance();
        } else if (ProcessorTypeConstants.ProcessorTypes.TRANSACTION.toString().equalsIgnoreCase(processorType)) {
            return TransactionProcessor.getInstance();
        }

        throw new LedgerManagerRuntimeException();
    }

}
