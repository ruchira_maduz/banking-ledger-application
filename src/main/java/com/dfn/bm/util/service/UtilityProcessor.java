package com.dfn.bm.util.service;

import bsh.util.Util;
import com.dfn.bm.actors.messages.ServicesBaseResponse;
import com.dfn.bm.beans.Account;
import com.dfn.bm.beans.User;
import com.dfn.bm.connector.database.DBHandler;
import com.dfn.bm.connector.database.DBUtilStore;
import com.dfn.bm.helper.ServiceTypes;
import com.dfn.bm.util.ServicesUtills;
import com.dfn.bm.util.Utils;
import com.dfn.bm.util.exception.LedgerManagerRuntimeException;
import com.dfn.bm.util.json.JSONParser;
import com.dfn.bm.util.security.PasswordEncryptor;
import com.dfn.bm.util.settings.PaymentGateWayEndPointSettings;
import com.dfn.bm.util.settings.Settings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdbi.v3.core.Jdbi;
import org.json.simple.JSONObject;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class UtilityProcessor implements ServiceProcessor {

    private static final Logger LOGGER = LogManager.getLogger(UtilityProcessor.class);
    private static UtilityProcessor INSTANCE = new UtilityProcessor();
    private Jdbi dbi = DBUtilStore.getInstance().getDbiInstance().installPlugins();
    private SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

    private UtilityProcessor() {

    }

    public static UtilityProcessor getInstance() {
        return INSTANCE;
    }


    @Override
    public ServicesBaseResponse process(Object params, int functionId) {
        switch (functionId) {

            case ServiceTypes.ADD_USER:
                return addUser(params);
            case ServiceTypes.ADD_ACCOUNT:
                return addAccount(params);
            case ServiceTypes.BILL_PAYMENT:
                return processBillPayment(params);


        }
        throw new LedgerManagerRuntimeException("Unknown Function Type Function ID: " + functionId);
    }

    private ServicesBaseResponse addUser(Object params) {

        ServicesBaseResponse response = new ServicesBaseResponse();

        JSONObject request = (JSONObject) params;

        String password = request.get("password") != null ? request.get("password").toString() : null;
        String firstname = request.get("firstName") != null ? request.get("firstName").toString() : null;
        String lastname = request.get("lastName") != null ? request.get("lastName").toString() : null;
        Integer userStatus = ((Number) request.get("userStatus")).intValue();
        String telephone = request.get("telephone") != null ? request.get("telephone").toString() : null;
        String nin = request.get("nin") != null ? request.get("nin").toString() : null;


        try {
            User user = new User(Utils.getUserName(firstname, lastname), PasswordEncryptor.getSha256SecurePassword(password), firstname, lastname, userStatus, telephone, nin);
            DBHandler.getInstance().insertUser(user);
            ServicesUtills.generateSuccussResponse(response, null);
        } catch (Exception e) {
            throw new LedgerManagerRuntimeException(e);
        }

        return response;
    }

    private ServicesBaseResponse addAccount(Object params) {
        ServicesBaseResponse response = new ServicesBaseResponse();

        JSONObject request = (JSONObject) params;

        int customerID = ((Number) request.get("customerID")).intValue();
        int type = ((Number) request.get("type")).intValue();
        double balance = ((Number) request.get("balance")).doubleValue();
        String accountID = UUID.randomUUID().toString();

        try {
            Account account = new Account(accountID, customerID, balance, type);
            DBHandler.getInstance().addToAccountTable(account);
            ServicesUtills.generateSuccussResponse(response, null);
        } catch (Exception e) {
            throw new LedgerManagerRuntimeException(e);
        }
        return response;
    }

    private ServicesBaseResponse processBillPayment(Object params) {

        ServicesBaseResponse response = new ServicesBaseResponse();
        JSONObject request = (JSONObject) params;

        int payeeID = ((Number) request.get("payeeID")).intValue();
        double amount = ((Number) request.get("amount")).doubleValue();
        String accountID = request.get("accountID") != null ? request.get("accountID").toString() : null;

        PaymentGateWayEndPointSettings endPointSettings = Settings.getInstance().getPaymentGateWayEndPointSettings();

        try {
            Account account = DBHandler.getInstance().getAccountByAccountID(accountID);
            double balance = account.getBalance();
            if (balance > amount) {

                String requestString = JSONParser.getInstance().toJson(request);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                headers.put("Accept", "application/json");
                String responseString = ServicesUtills.sendHTTPPostRequest(requestString, headers, endPointSettings.getHost(), endPointSettings.getPort(), "/PaymentGateWayAPI");
                if (responseString.equalsIgnoreCase("OK")) {
                    JSONObject finalResponse = sendTransactionRequest(request, headers, endPointSettings);
                    ServicesUtills.generateSuccussResponse(response, finalResponse);
                } else {
                    ServicesUtills.generateErrorResponse(response, responseString);
                }
            }
        } catch (Exception e) {
            throw new LedgerManagerRuntimeException(e);
        }
        return response;
    }

    private JSONObject sendTransactionRequest(JSONObject object, Map<String, String> headers, PaymentGateWayEndPointSettings endPointSettings) {
        object.put("transactionType", ServiceTypes.WITHDRAWAL_TRANSACTION);
        object.put("description", "Bill Payment");
        object.put("functionId", 1);

        String requestString = JSONParser.getInstance().toJson(object);
        String responseString = ServicesUtills.sendHTTPPostRequest(requestString, headers, endPointSettings.getHost(), endPointSettings.getPort(), "/TransactionAPI");
        return JSONParser.getInstance().fromJson(responseString, JSONObject.class);
    }
}
