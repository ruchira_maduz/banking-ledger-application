package com.dfn.bm.util.service;

public class ProcessorTypeConstants {

    public enum ProcessorTypes {
        LOGIN("LOGIN"),
        TRANSACTION("TRANSACTION"),
        UTILITY("UTILITY");
        private final String name;

        ProcessorTypes(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}
