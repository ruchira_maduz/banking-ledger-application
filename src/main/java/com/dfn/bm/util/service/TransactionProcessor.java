package com.dfn.bm.util.service;

import com.dfn.bm.actors.messages.ServicesBaseResponse;
import com.dfn.bm.beans.Account;
import com.dfn.bm.beans.Transaction;
import com.dfn.bm.connector.database.DBHandler;
import com.dfn.bm.connector.database.DBUtilStore;
import com.dfn.bm.helper.ServiceTypes;
import com.dfn.bm.util.ServicesUtills;
import com.dfn.bm.util.exception.LedgerManagerRuntimeException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.json.simple.JSONObject;

import java.util.List;

public class TransactionProcessor implements ServiceProcessor {

    private static final Logger LOGGER = LogManager.getLogger(TransactionProcessor.class);
    private static TransactionProcessor INSTANCE = new TransactionProcessor();
    private Jdbi dbi = DBUtilStore.getInstance().getDbiInstance().installPlugins();

    private TransactionProcessor() {

    }

    public static TransactionProcessor getInstance() {
        return INSTANCE;
    }

    @Override
    public ServicesBaseResponse process(Object params, int functionId) {

        switch (functionId) {
            case ServiceTypes.ADD_TRANSACTION:
                return addTransaction(params);
            case ServiceTypes.SHOW_TRANSACTION:
                return processAllTransactions(params);
            case ServiceTypes.FILTER_TRANSACTION:
                return processAllTransactionsByMonth(params);

        }
        throw new LedgerManagerRuntimeException("Unknown Function Type Function ID: " + functionId);
    }

    private ServicesBaseResponse addTransaction(Object params) {
        ServicesBaseResponse response = new ServicesBaseResponse();

        JSONObject request = (JSONObject) params;
        int transactionType = ((Number) request.get("transactionType")).intValue();
        double amount = ((Number) request.get("amount")).doubleValue();
        String accountID = request.get("accountID") != null ? request.get("accountID").toString() : null;
        String description = request.get("description") != null ? request.get("description").toString() : null;

        try {
            Account account = DBHandler.getInstance().getAccountByAccountID(accountID);
            double balance = account.getBalance();
            if (transactionType == ServiceTypes.DEPOSIT_TRANSACTION) {
                balance = balance + amount;
            } else if (balance > amount) {
                balance = balance - amount;
            } else {
                throw new LedgerManagerRuntimeException("This action is Not Allowed");
            }

            Transaction transaction = new Transaction(accountID, transactionType, amount, balance, description);
            DBHandler.getInstance().insertTransaction(transaction);
            DBHandler.getInstance().updateAccountTable(balance, accountID);

            ServicesUtills.generateSuccussResponse(response, null);
        } catch (Exception e) {
            throw new LedgerManagerRuntimeException(e);
        }

        return response;
    }


    private ServicesBaseResponse processAllTransactions(Object params) {
        ServicesBaseResponse response = new ServicesBaseResponse();

        JSONObject request = (JSONObject) params;
        String accountID = request.get("accountID") != null ? request.get("accountID").toString() : null;
        try {
            List<Transaction> transactionList = DBHandler.getInstance().getTransactions(accountID);
            ServicesUtills.generateSuccussResponse(response, ServicesUtills.getListObject(transactionList));

        } catch (Exception e) {
            throw new LedgerManagerRuntimeException();
        }
        return response;
    }

    private ServicesBaseResponse processAllTransactionsByMonth(Object params) {
        ServicesBaseResponse response = new ServicesBaseResponse();

        JSONObject request = (JSONObject) params;
        String from = request.get("from") != null ? request.get("from").toString() : null;
        String to = request.get("to") != null ? request.get("to").toString() : null;
        String accountID = request.get("accountID") != null ? request.get("accountID").toString() : null;

        try {
            List<Transaction> transactionList = DBHandler.getInstance().getTransactions(from, accountID, to);
            ServicesUtills.generateSuccussResponse(response, ServicesUtills.getListObject(transactionList));

        } catch (Exception e) {
            throw new LedgerManagerRuntimeException();
        }
        return response;
    }

}
