package com.dfn.bm.util;

/**
 * Enum for storing pre-defined actor names for easy access.
 * CAUTION! - If there are more than one actor of a certain type,
 * use the enum value with a suffix, to make the Actor name Unique.
 */
public enum ActorName {

    LEDGER_MANAGER_SUPERVISOR("LedgerManagerSupervisor"),
    TRANSACTION_HTTP_ENDPOINT_SUPERVISOR("TransactionHttpEndPointSupervisor"),
    UTILITY_HTTP_ENDPOINT_SUPERVISOR("UtilityHttpEndPointSupervisor"),
    LOGIN_LOGOUT_HTTP_ENDPOINT_SUPERVISOR("LoginLogOutHttpEndPointSupervisor"),
    SERVICE_REQUEST_WORKER("ServiceRequestWorker");

    /**
     * Actor's name.
     */
    private final String name;

    /**
     * @param name - Actor's name.
     */
    ActorName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
