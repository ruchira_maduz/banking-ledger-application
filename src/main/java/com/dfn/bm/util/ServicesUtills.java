package com.dfn.bm.util;

import com.dfn.bm.beans.Transaction;
import com.dfn.bm.beans.User;
import com.dfn.bm.connector.database.DBHandler;
import com.dfn.bm.util.exception.LedgerManagerRuntimeException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

public class ServicesUtills {

    private static final Logger LOGGER = LogManager.getLogger(ServicesUtills.class);

    public static void generateErrorResponse(JSONObject object, Object errorConstant) {
        JSONObject errorObject = new JSONObject();
        object.put("resStatus", 0);
        errorObject.put("Error", "Error: " + errorConstant);
        object.put("responData", errorObject);
    }

    public static void generateSuccussResponse(JSONObject object, Object message) {
        object.put("resStatus", true);
        object.put("responseObj", message);
    }

    public static JSONObject getListObject(List<Transaction> transactionList) {

        JSONObject response = new JSONObject();
        response.put("transactionList", transactionList);
        return response;
    }

    public static boolean validateToken(String userName, String token) {
        if (token == null) {
            User userObject = DBHandler.getInstance().retrieveUserSessionObject(userName);
            if (userObject == null) {
                return false;
            } else {
                return true;
            }
        } else {
            try {
                String signature = JWTPIUtils.decodeJWT(token).getSignature();
                String jwtSignature = token.split("\\.")[2];
                if (signature.equalsIgnoreCase(jwtSignature)) {
                    return true;
                } else {
                    return false;
                }
            } catch (Exception e) {
                return false;
            }
        }
    }

    public static String sendHTTPPostRequest(String request, Map<String, String> headers, String host, int port, String endpoint) {
        HttpURLConnection con = null;
        try {
            byte[] postData = request.getBytes(StandardCharsets.UTF_8);
            String url = "http://" + host
                    + ":" + port + (endpoint.contains("/") ? endpoint : "/" + endpoint);

            URL urlObj = new URL(url);
            con = (HttpURLConnection) urlObj.openConnection();
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            for (String key : headers.keySet()) {
                con.setRequestProperty(key, headers.get(key));
            }
            con.setDoOutput(true);

            try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
                wr.write(postData);
            }

            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream()))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                LOGGER.info("Request sent to Server @ {} Response: {}", url, response);
                return response.toString();
            }

        } catch (Exception e) {
            LOGGER.error("Error while sending request: {}", e, e);
            throw new LedgerManagerRuntimeException(e);
        } finally {
            if (con != null) {
                con.disconnect();
            }
        }
    }

}
