package com.dfn.bm.util.exception;

/**
 * Specialized Run Time Exception for Run time exceptions thrown by Recrutement Manager.
 */

public class LedgerManagerRuntimeException extends RuntimeException {

    public LedgerManagerRuntimeException() {

    }

    public LedgerManagerRuntimeException(Exception e) {
        super(e);
    }

    public LedgerManagerRuntimeException(String message) {
        super(message);
    }
}
