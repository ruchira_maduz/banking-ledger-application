package com.dfn.bm.util.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.LongSerializationPolicy;

import java.util.Map;

/**
 * Common JSON Parser class for maintaining single json parsing rules throughout Athena.
 */
public final class JSONParser implements JSONUtils {

    private static final JSONParser INSTANCE = new JSONParser();
    private static Gson GSON = new GsonBuilder()
            .setPrettyPrinting()
            .disableHtmlEscaping()
            .setLongSerializationPolicy(LongSerializationPolicy.DEFAULT)
            .create();

    private JSONParser() {

    }

    public static void setGSONInstance(GsonBuilder gsonBuilder) {
        GSON = gsonBuilder
                .setPrettyPrinting()
                .disableHtmlEscaping()
                .setLongSerializationPolicy(LongSerializationPolicy.DEFAULT)
                .create();
    }

    public static Gson getGsonInstance() {
        return GSON;
    }

    public static JSONParser getInstance() {
        return INSTANCE;
    }

    public <T> T fromJson(Object message, Class<T> type) {
        String jsonString = JSONParser.getInstance().toJson(message, Map.class);
        return this.fromJson(jsonString, type);
    }

    @Override
    public String toJson(Object data) {
        return GSON.toJson(data);
    }

    @Override
    public <T> String toJson(Object data, Class<T> classOfT) {
        return GSON.toJson(data, classOfT);
    }

    @Override
    public <T> T fromJson(String json, Class<T> classOfT) {
        return GSON.fromJson(json, classOfT);
    }

//    public <T> T test(FileReader reader, Class<T> classOfT){
//        Object obj = GSON.fromJson(reader, classOfT);
//    }
}
