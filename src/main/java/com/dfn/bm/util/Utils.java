/**
 * Utility package of Recruitment Manager Server
 */
package com.dfn.bm.util;

import com.dfn.bm.util.exception.LedgerManagerRuntimeException;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Main Utility class for performing various common util actions.
 */
public final class Utils {

    private static final Logger LOGGER = LogManager.getLogger(Utils.class);


    public static boolean isNullOrEmptyString(String string) {
        return string == null || string.trim().isEmpty();
    }

    private Utils() {

    }

    public static String getUserName(String firstName, String lastName) {
        return firstName.trim() + getRandomString(5) + lastName;
    }

    public static InputStream getFileResourceInputStream(String filePath) {
        InputStream inputStream;
        try {
            inputStream = new FileInputStream(filePath);
        } catch (Exception e) {
            throw new LedgerManagerRuntimeException();
        }
        return inputStream;
    }

    public static String getRandomString(int length) {
        return UUID.randomUUID().toString().substring(0, length);
    }

}
