package com.dfn.bm.beans;

public class Account {

    private int id;
    private String accountID;
    private int customerID;
    private double balance;
    private int type;


    public Account() {
    }

    public Account(String accountID, int customerID, double balance, int type) {
        this.accountID = accountID;
        this.customerID = customerID;
        this.balance = balance;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAccountID() {
        return accountID;
    }

    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
