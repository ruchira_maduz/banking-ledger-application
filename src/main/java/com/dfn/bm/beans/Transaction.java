package com.dfn.bm.beans;

public class Transaction {

    private int id;
    private String accountID;
    private int transactionType;
    private double amount;
    private String date;
    private double balance;
    private String description;

    public Transaction() {
    }

    public Transaction(int id, int transactionType, int transactionCategory, double amount, String date, String description) {
        this.id = id;
        this.transactionType = transactionType;
        this.amount = amount;
        this.date = date;
        this.description = description;
    }

    public Transaction(String accountID, int transactionType, double amount, double balance, String description) {
        this.accountID = accountID;
        this.transactionType = transactionType;
        this.amount = amount;
        this.description = description;
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(int transactionType) {
        this.transactionType = transactionType;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAccountID() {
        return accountID;
    }

    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
