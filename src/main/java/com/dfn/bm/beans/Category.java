package com.dfn.bm.beans;

public class Category {

    private int Id;
    private int transactionCategory;
    private String name;


    public Category() {
    }

    public Category(int id, int transactionCategory, String name) {
        Id = id;
        this.transactionCategory = transactionCategory;
        this.name = name;
    }


    public Category(int transactionCategory, String name) {
        this.transactionCategory = transactionCategory;
        this.name = name;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getTransactionCategory() {
        return transactionCategory;
    }

    public void setTransactionCategory(int transactionCategory) {
        this.transactionCategory = transactionCategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
