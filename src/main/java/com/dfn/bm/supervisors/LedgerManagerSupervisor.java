package com.dfn.bm.supervisors;

import akka.actor.typed.*;
import akka.actor.typed.javadsl.*;
import com.dfn.bm.actors.messages.StartHttpPServer;
import com.dfn.bm.actors.messages.StartSupervisorCommand;
import com.dfn.bm.connector.httpserver.HTTPEndpoint;
import com.dfn.bm.util.ActorName;
import com.dfn.bm.util.ActorUtils;
import com.dfn.bm.util.message.Command;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LedgerManagerSupervisor extends AbstractBehavior<Command> {


    private static final Logger LOGGER = LogManager.getLogger(LedgerManagerSupervisor.class);

    public LedgerManagerSupervisor(ActorContext<Command> context) {
        super(context);
    }

    public static Behavior<Command> create() {
        LOGGER.info("Ledger Manager Supervisor started.");
        return Behaviors.setup(LedgerManagerSupervisor::new);
    }



    public Receive<Command> createReceive() {
        return newReceiveBuilder().
                onMessage(StartSupervisorCommand.class, this::startSupervisors).
                onSignal(PreRestart.class, signal -> onPreStart()).
                onSignal(ChildFailed.class, signal -> onChildFailed()).
                onSignal(PostStop.class, signal -> onPostStop()).build();
    }

    private Behavior<Command> startSupervisors(StartSupervisorCommand command) {

        ActorRef<Command> transactionHttpEndPointSupervisor = getContext().
                spawn(TransactionHTTPEndpointSupervisor.create(), ActorName.TRANSACTION_HTTP_ENDPOINT_SUPERVISOR.toString());
        ActorUtils.setTransactionEndPointSupervisor(transactionHttpEndPointSupervisor);
        transactionHttpEndPointSupervisor.tell(new StartHttpPServer());
        LOGGER.info("Transaction HTTP EndPoint Supervisor is created {}", transactionHttpEndPointSupervisor);

        ActorRef<Command> utilityHttpEndPointSupervisor = getContext().
                spawn(UtilityHTTPEndpointSupervisor.create(), ActorName.UTILITY_HTTP_ENDPOINT_SUPERVISOR.toString());
        ActorUtils.setUtilityEndPointSupervisor(utilityHttpEndPointSupervisor);
        utilityHttpEndPointSupervisor.tell(new StartHttpPServer());

        ActorRef<Command> loginLogoutHttpEndPointSupervisor = getContext().
                spawn(LoginLogOutHTTPEndPointSupervisor.create(), ActorName.LOGIN_LOGOUT_HTTP_ENDPOINT_SUPERVISOR.toString());
        ActorUtils.setLoginLogoutEndPointSupervisor(loginLogoutHttpEndPointSupervisor);
        loginLogoutHttpEndPointSupervisor.tell(new StartHttpPServer());

        HTTPEndpoint.getSharedInstance().addTransactionAPIRequestRoute();
        HTTPEndpoint.getSharedInstance().addUtilityAPIRequestRoute();
        HTTPEndpoint.getSharedInstance().addPaymentGateWayAPIRequestRoute();
        HTTPEndpoint.getSharedInstance().addLoginAPIRequestRoute();

        return Behaviors.same();
    }

    public LedgerManagerSupervisor onPostStop() {
        HTTPEndpoint.getSharedInstance().unbindThePort();
        LOGGER.info("Ledger Manager Supervisor is stopped.");
        System.exit(1);
        return this;
    }

    public LedgerManagerSupervisor onPreStart() {
        LOGGER.info("Ledger Manager Supervisor is PreStart.");
        return this;
    }

    public LedgerManagerSupervisor onChildFailed() {
        LOGGER.warn("Ledger Manager Supervisor Child failed");
        return this;
    }
}
