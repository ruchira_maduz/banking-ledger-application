package com.dfn.bm.helper;

public class ServiceTypes {

    public static final int DEPOSIT_TRANSACTION = 1;
    public static final int WITHDRAWAL_TRANSACTION = 2;
    public static final String EXPENDITURE_TRANSACTION_SERVICE = "EXPENSE";
    public static final String TRANSACTION_CATEGORY_SERVICE = "CATEGORY";
    public static final String BUDGET_SERVICE = "BUDGET";

    public static final int ADD_TRANSACTION = 1;
    public static final int SHOW_TRANSACTION = 2;
    public static final int FILTER_TRANSACTION = 3;


    public static final int ADD_ACCOUNT = 1;
    public static final int BILL_PAYMENT = 2;
    public static final int ADD_USER = 3;




}
