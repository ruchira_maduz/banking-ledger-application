/**
 * The bootstrapper package manages initial configuration and successful initialization of the Server.
 * This package reads the configuration files written by the Administrators of Athena Server and spawns the necessary
 * actors which performs the required tasks.
 * Ex: If a brokerage only wants features such as conditional orders, VWAP, TWAP the bootstrapper will only start
 * necessary actors for those features of Athena Server.
 */
package com.dfn.bm.bootstrap;


import com.dfn.bm.actors.messages.StartSupervisorCommand;
import com.dfn.bm.connector.database.DBContentHandler;
import com.dfn.bm.connector.httpserver.HTTPEndpoint;
import com.dfn.bm.supervisors.LedgerManagerSupervisor;
import com.dfn.bm.util.ActorName;
import com.dfn.bm.util.ActorUtils;
import com.dfn.bm.util.SettingsNotFoundException;
import com.dfn.bm.util.SettingsReader;
import com.dfn.bm.util.message.Command;
import com.dfn.bm.util.settings.Settings;
import com.typesafe.config.Config;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Starting point of the Ledger Manager Server.
 */
public final class StartServer {

    private static final Logger LOGGER = LogManager.getLogger(StartServer.class);

    /**
     * Default settings file for Ledger Manager  server.
     */
    private static final String DEFAULT_SETTINGS_FILE = "config/Settings.yml";

    /**
     * Default settings file for Ledger Manager  configuration.
     */
    private static final String DEFAULT_AKKA_SETTINGS_FILE = "config/Akka.conf";

    /**
     * Default constructor.
     */
    private StartServer() {

    }

    /**
     * Main class.
     *
     * @param args - no args are required to run Ledger Manager  with YML configuration file.
     */
    public static void main(final String[] args) {

        LOGGER.info("Reading Ledger Manager Config from: {}", DEFAULT_AKKA_SETTINGS_FILE);

        try {
            Settings settings = SettingsReader.loadSettings(DEFAULT_SETTINGS_FILE);

            Settings.getInstance().setHttpEndpointSettings(settings.getHttpEndpointSettings());
            Settings.getInstance().setDbSettings(settings.getDbSettings());
            Settings.getInstance().setSocketEndpointSettings(settings.getSocketEndpointSettings());
            Settings.getInstance().setPaymentGateWayEndPointSettings(settings.getPaymentGateWayEndPointSettings());

            Config config = SettingsReader.loadAkkaSettings(DEFAULT_AKKA_SETTINGS_FILE);
            LOGGER.info("Creating Ledger Manager  Actor System: ");

            akka.actor.typed.ActorSystem<Command> rootSupervisor = akka.actor.typed.ActorSystem.create(LedgerManagerSupervisor.create(),
                    ActorName.LEDGER_MANAGER_SUPERVISOR.toString(), config);

            ActorUtils.setRootSupervisor(rootSupervisor);
            rootSupervisor.tell(new StartSupervisorCommand());
            HTTPEndpoint.getSharedInstance().initialize();

            ActorUtils.setRootSupervisor(rootSupervisor);
            LOGGER.info("Actor system is created {} address {}", rootSupervisor, rootSupervisor.address());


            String dbType = Settings.getInstance().getDbSettings().getDatabaseType();
            LOGGER.info("Creating Default DB Schema: {}", dbType);
            DBContentHandler.getInstance().initializeDBContent();
            LOGGER.info("Launched Ledger Manager  Engine Supervisor");



        } catch (SettingsNotFoundException e) {
            LOGGER.error("Settings not found", e);
        }

    }
}
