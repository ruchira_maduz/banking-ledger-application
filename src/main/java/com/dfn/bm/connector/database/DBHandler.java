package com.dfn.bm.connector.database;

import com.dfn.bm.beans.Account;
import com.dfn.bm.beans.Transaction;
import com.dfn.bm.beans.User;
import com.dfn.bm.connector.database.dao.AccountDetailsDAO;
import com.dfn.bm.connector.database.dao.TransactionDetailDAO;
import com.dfn.bm.connector.database.dao.UserDAO;
import com.dfn.bm.util.exception.LedgerManagerRuntimeException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.List;

/**
 * DB Handler.
 */

public final class DBHandler {

    private static final Logger LOGGER = LogManager.getLogger(DBHandler.class);
    private SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    private static final DBHandler INSTANCE = new DBHandler();
    private Jdbi dbi = DBUtilStore.getInstance().getDbiInstance().installPlugins();

    private DBHandler() {

    }

    public static DBHandler getInstance() {
        return INSTANCE;
    }


    public void insertTransaction(Transaction transaction) {
        String date = formatter.format(Date.from(Instant.now()));

        try (Handle handle = dbi.open()) {
            handle.begin();
            try {
                TransactionDetailDAO transactionDetailDAO = handle.attach(TransactionDetailDAO.class);
                transactionDetailDAO.addToTransactionTable(transaction.getTransactionType(),
                        transaction.getAccountID(), transaction.getAmount(), transaction.getBalance(), date, transaction.getDescription());

                handle.commit();
            } catch (Exception e) {
                handle.rollback();
                throw new LedgerManagerRuntimeException(e);
            }
        } catch (Exception e) {
            throw new LedgerManagerRuntimeException(e);
        }
    }

    public List<Transaction> getTransactions(String from, String to, String accountID) {
        try (Handle handle = dbi.open()) {
            TransactionDetailDAO transactionDetailDAO = handle.attach(TransactionDetailDAO.class);
            return transactionDetailDAO.getTransactions(from, accountID, to);

        } catch (Exception e) {
            throw new LedgerManagerRuntimeException(e);
        }
    }

    public List<Transaction> getTransactions(String accountID) {
        try (Handle handle = dbi.open()) {
            TransactionDetailDAO transactionDetailDAO = handle.attach(TransactionDetailDAO.class);
            return transactionDetailDAO.getAllTransactions(accountID);

        } catch (Exception e) {
            throw new LedgerManagerRuntimeException(e);
        }
    }

    public void addToAccountTable(Account account) {
        try (Handle handle = dbi.open()) {
            handle.begin();
            try {
                AccountDetailsDAO accountDetailsDAO = handle.attach(AccountDetailsDAO.class);
                accountDetailsDAO.addToAccountTable(account.getCustomerID(), account.getAccountID(),
                        account.getBalance(), account.getType());

                handle.commit();
            } catch (Exception e) {
                handle.rollback();
                throw new LedgerManagerRuntimeException(e);
            }
        } catch (Exception e) {
            throw new LedgerManagerRuntimeException(e);
        }
    }

    public Account getAccountByAccountID(String accountID) {
        try (Handle handle = dbi.open()) {
            AccountDetailsDAO accountDetailsDAO = handle.attach(AccountDetailsDAO.class);
            return accountDetailsDAO.getAccountByAccountID(accountID);
        } catch (Exception e) {
            throw new LedgerManagerRuntimeException(e);
        }
    }

    public void updateAccountTable(double balance, String accountID) {
        try (Handle handle = dbi.open()) {
            handle.begin();
            try {
                AccountDetailsDAO accountDetailsDAO = handle.attach(AccountDetailsDAO.class);
                accountDetailsDAO.updateAccountTable(balance, accountID);

                handle.commit();
            } catch (Exception e) {
                handle.rollback();
                throw new LedgerManagerRuntimeException(e);
            }
        } catch (Exception e) {
            throw new LedgerManagerRuntimeException(e);
        }
    }


    public void insertUser(User user) {
        try (Handle handle = dbi.open()) {
            handle.begin();

            String date = formatter.format(Date.from(Instant.now()));
            user.setCreatedDate(date);

            try {
                UserDAO userDAO = handle.attach(UserDAO.class);
                userDAO.insertUser(user);

                handle.commit();
            } catch (Exception e) {
                handle.rollback();
                throw new LedgerManagerRuntimeException(e);
            }
        } catch (Exception e) {
            LOGGER.error("Error: {}", e, e);
            throw new LedgerManagerRuntimeException(e);
        }
    }

    public User retrieveUserSessionObject(String userName) {
        User user = null;
        try (Handle handle = dbi.open()) {
            UserDAO userDAO = handle.attach(UserDAO.class);
            user = userDAO.retrieveUserSessionAndPassword(userName);

        } catch (Exception e) {
            LOGGER.error("Error: {}", e, e);
            throw new LedgerManagerRuntimeException(e);
        }
        return user;
    }

    public User getUserObjectByUserName(String userName) {
        User userObject = null;
        try (Handle handle = dbi.open()) {
            UserDAO userDAO = handle.attach(UserDAO.class);
            userObject = userDAO.getUsersByUsername(userName);
        }
        return userObject;
    }

}

