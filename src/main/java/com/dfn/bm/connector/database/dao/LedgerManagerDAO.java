package com.dfn.bm.connector.database.dao;

import org.jdbi.v3.sqlobject.statement.SqlUpdate;

/**
 * Data Access Object for Creating Tables of DB.
 */

public interface LedgerManagerDAO {

    @SqlUpdate("CREATE TABLE IF NOT EXISTS USER_TABLE(\n"
            + "ID INT(11) NOT NULL AUTO_INCREMENT,\n"
            + "USERNAME VARCHAR(100) NOT NULL, \n"
            + "FIRST_NAME VARCHAR(100) NOT NULL, \n"
            + "LAST_NAME VARCHAR(100) NOT NULL, \n"
            + "PASSWORD VARCHAR(100) NOT NULL, \n"
            + "TELEPHONE VARCHAR(100) NOT NULL, \n"
            + "NIN VARCHAR(100) NOT NULL, \n"
            + "CREATED_DATE VARCHAR(100) NOT NULL, \n"
            + "LAST_REQUEST_TIME VARCHAR(100) NULL, \n"
            + "SOCKET_SESSION_ID VARCHAR(100) , \n"
            + "SESSION_ID VARCHAR(100) , \n"
            + "IS_LOGGED_IN INT(5) NOT NULL, \n"
            + "USER_STATUS INT(5)  NOT NULL, \n"
            + "PRIMARY KEY (ID))")
    void createUserTable();

    @SqlUpdate("CREATE TABLE IF NOT EXISTS ACCOUNT_TABLE(\n"
            + "ID INT(11) NOT NULL AUTO_INCREMENT,\n"
            + "ACCOUNT_ID VARCHAR(100) NOT NULL, \n"
            + "CUSTOMER_ID INT(11) NOT NULL, \n"
            + "BALANCE DOUBLE NOT NULL, \n"
            + "TYPE INT(11) NOT NULL, \n"
            + "PRIMARY KEY (ID))")
    void createAccountsTable();

    @SqlUpdate("CREATE TABLE IF NOT EXISTS TRANSACTION_TABLE(\n"
            + "ID INT(11) NOT NULL AUTO_INCREMENT,\n"
            + "ACCOUNT_ID VARCHAR(100) NOT NULL, \n"
            + "TRANSACTION_TYPE INT(11) NOT NULL, \n"
            + "AMOUNT DOUBLE NOT NULL, \n"
            + "BALANCE DOUBLE NOT NULL, \n"
            + "DATE DATETIME NOT NULL, \n"
            + "DESCRIPTION TEXT NOT NULL, \n"
            + "PRIMARY KEY (ID))")
    void createTransactionsTable();



}
