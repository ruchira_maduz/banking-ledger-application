package com.dfn.bm.connector.database.dao;

import com.dfn.bm.beans.Transaction;
import com.dfn.bm.connector.database.mapper.transactionmapper.TransactionMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.statement.UseRowMapper;

import java.util.List;

public interface TransactionDetailDAO {

    @SqlUpdate("INSERT INTO TRANSACTION_TABLE (ACCOUNT_ID, TRANSACTION_TYPE, AMOUNT, BALANCE, DATE, DESCRIPTION)\n"
            + "VALUES (:ACCOUNT_ID, :TRANSACTION_TYPE, :AMOUNT, :BALANCE, :DATE, :DESCRIPTION);")
    void addToTransactionTable(@Bind("TRANSACTION_TYPE") int transactionType,
                               @Bind("ACCOUNT_ID") String accountID,
                               @Bind("AMOUNT") double amount,
                               @Bind("BALANCE") double balance,
                               @Bind("DATE") String date,
                               @Bind("DESCRIPTION") String description);

    @SqlQuery("SELECT * FROM TRANSACTION_TABLE WHERE ((:TO IS NULL) OR (DATE <= :TO))  AND ((:FROM IS NULL) OR (DATE >= :FROM)) AND ACCOUNT_ID = :ACCOUNT_ID ")
    @UseRowMapper(TransactionMapper.class)
    List<Transaction> getTransactions(@Bind("FROM") String from,
                                      @Bind("ACCOUNT_ID") String accountID,
                                      @Bind("TO") String to);

    @SqlQuery("SELECT * FROM TRANSACTION_TABLE WHERE ACCOUNT_ID = :ACCOUNT_ID ")
    @UseRowMapper(TransactionMapper.class)
    List<Transaction> getAllTransactions(@Bind("ACCOUNT_ID") String accountID);


}
