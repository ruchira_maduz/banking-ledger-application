package com.dfn.bm.connector.database.mapper.usermapper;

import com.dfn.bm.beans.User;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserPasswordSessionMapper implements RowMapper<User> {

    @Override
    public User map(ResultSet rs, StatementContext ctx) throws SQLException {
        User user = new User();
        user.setUsername(rs.getString("USERNAME"));
        user.setPassword(rs.getString("PASSWORD"));
        user.setSessionId(rs.getString("SESSION_ID"));
        user.setSocketSessionId(rs.getString("SOCKET_SESSION_ID"));

        return user;
    }
}
