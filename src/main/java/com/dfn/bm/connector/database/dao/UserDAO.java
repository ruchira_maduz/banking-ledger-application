package com.dfn.bm.connector.database.dao;

import com.dfn.bm.beans.User;
import com.dfn.bm.connector.database.mapper.usermapper.UserPasswordSessionMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.statement.UseRowMapper;

import java.util.Date;

public interface UserDAO {

    @SqlUpdate("INSERT INTO USER_TABLE(USERNAME, PASSWORD, FIRST_NAME, LAST_NAME, USER_STATUS," +
            " TELEPHONE, NIN, " +
            " CREATED_DATE) VALUES(:user.username, :user.password, :user.firstname, :user.lastname," +
            " :user.userStatus, :user.telephone, " +
            " :user.nin, :user.createdDate)")
    void insertUser(@BindBean("user") User user);

    @SqlUpdate("UPDATE USER_TABLE SET  LAST_REQUEST_TIME =:LAST_REQUEST_TIME , "
            + " SESSION_ID = :SESSION_ID, SOCKET_SESSION_ID = :SOCKET_SESSION_ID WHERE USERNAME = :USERNAME ")
    void updateDBOnRequestReceived(@Bind("USERNAME") String userName,
                                   @Bind("SESSION_ID") String sessionId,
                                   @Bind("SOCKET_SESSION_ID") String socketSessionId,
                                   @Bind("LAST_REQUEST_TIME") Date lastRequestTime);

    @SqlQuery("SELECT USERNAME , PASSWORD, SESSION_ID, SOCKET_SESSION_ID FROM USER_TABLE WHERE USERNAME = :USERNAME ")
    @UseRowMapper(UserPasswordSessionMapper.class)
    User retrieveUserSessionAndPassword(@Bind("USERNAME") String userName);

    @SqlQuery("SELECT * FROM USER_TABLE WHERE USERNAME = :USERNAME ")
    @UseRowMapper(UserPasswordSessionMapper.class)
    User getUsersByUsername(@Bind("USERNAME") String userName);


}
