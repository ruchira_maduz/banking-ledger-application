package com.dfn.bm.connector.database.dao;

import com.dfn.bm.beans.Account;
import com.dfn.bm.connector.database.mapper.accountmapper.AccountMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.statement.UseRowMapper;

public interface AccountDetailsDAO {

    @SqlUpdate("INSERT INTO ACCOUNT_TABLE (ACCOUNT_ID, CUSTOMER_ID, BALANCE, TYPE)\n"
            + "VALUES (:ACCOUNT_ID, :CUSTOMER_ID, :BALANCE, :TYPE);")
    void addToAccountTable(@Bind("CUSTOMER_ID") int customerID,
                               @Bind("ACCOUNT_ID") String accountID,
                               @Bind("BALANCE") double balance,
                               @Bind("TYPE") int type);


    @SqlUpdate("UPDATE ACCOUNT_TABLE SET  BALANCE = :BALANCE WHERE ACCOUNT_ID = :ACCOUNT_ID ")
    void updateAccountTable(@Bind("BALANCE") double balance,
                                   @Bind("ACCOUNT_ID") String accountID);

    @SqlQuery("SELECT * FROM ACCOUNT_TABLE WHERE ACCOUNT_ID = :ACCOUNT_ID ")
    @UseRowMapper(AccountMapper.class)
    Account getAccountByAccountID(@Bind("ACCOUNT_ID") String accountID);
}
