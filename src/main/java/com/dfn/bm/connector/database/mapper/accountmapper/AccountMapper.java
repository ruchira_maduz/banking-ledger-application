package com.dfn.bm.connector.database.mapper.accountmapper;

import com.dfn.bm.beans.Account;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AccountMapper implements RowMapper<Account> {

    @Override
    public Account map(ResultSet rs, StatementContext ctx) throws SQLException {
        Account account = new Account();
        account.setAccountID(rs.getString("ACCOUNT_ID"));
        account.setBalance(rs.getDouble("BALANCE"));
        account.setCustomerID(rs.getInt("CUSTOMER_ID"));
        account.setType(rs.getInt("TYPE"));

        return account;
    }
}
